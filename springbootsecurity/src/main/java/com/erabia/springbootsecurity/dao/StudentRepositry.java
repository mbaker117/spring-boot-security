package com.erabia.springbootsecurity.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erabia.springbootsecurity.bean.Student;

public interface StudentRepositry extends JpaRepository<Student, Integer> {

}
