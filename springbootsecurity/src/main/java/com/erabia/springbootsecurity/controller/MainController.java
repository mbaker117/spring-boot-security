package com.erabia.springbootsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.erabia.springbootsecurity.bean.Student;

import com.erabia.springbootsecurity.dao.StudentRepositry;


@Controller

public class MainController {
	@Autowired
	private StudentRepositry studentRepositry;

	@RequestMapping("/view")
	public String view(Model model) {
		List<Student> students = studentRepositry.findAll();
		model.addAttribute("students", students);
		return "studentPage";
	}

	@RequestMapping("/addStudentForm")
	public String add(Model model) {

		model.addAttribute("student", new Student());
		return "studentForm";
	}

	@PostMapping("saveStudent")
	public String saveStudent(@ModelAttribute("student") Student student, BindingResult bindingResult) {
		System.out.println(bindingResult);
		if (bindingResult.hasErrors())
			return "studentForm";
		if (student.getId() == 0)
			studentRepositry.save(student);
		else
			studentRepositry.save(student);

		return "redirect:/view";
	}

	@GetMapping("editStudent")
	public String editStudent(@RequestParam("studentId") int id, Model model) {

		model.addAttribute("student", studentRepositry.findById(id).get());
		return "studentForm";
	}

	@GetMapping("/deleteStudent")
	public String deleteStudent(@RequestParam("studentId") int id) {
		studentRepositry.deleteById(id);
		return "redirect:/view";
	}

	@InitBinder
	public void validate(WebDataBinder webDataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
}
